# README #

Recipes which demonstrate usage of ALUS GPU accelerated procedures.

### What it is for ###

* Simple examples requiring no knowledge of programming
* Missing pieces are processed utilizing SNAP GPT

### How do I get set up? ###

* Install SNAP GPT
* Download Sentinel 1 raw images one wants to process
* Currently only Ubuntu 18.04 (bionic) and Elementary OS 5.1 (Hera) are officially supported
* One can use docker container which has all the dependencies already set up - `docker pull cgialus/alus-infra:latest`

### Usage ###

`./coherence.sh <input 1> <input 2> <polarisation> <subswath> <burst index> <output_dir>`

Example:
`./coherence.sh /mnt/s1a/S1A_IW_SLC__1SDV_20200728T155625_20200728T155652_033657_03E699_1216.SAFE/manifest.safe /mnt/s1a/S1A_IW_SLC__1SDV_20200809T155626_20200809T155653_033832_03EC3F_9EB2.SAFE/manifest.safe VV IW2 2 /tmp`
