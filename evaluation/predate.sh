#!/bin/bash

while read line; do
  echo $(date +"%d-%m-%Y %T.%N %Z") ":" $line;    
done
