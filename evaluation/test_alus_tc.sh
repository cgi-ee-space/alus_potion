#!/bin/bash

set -e

for i in 1 2 3 4 5
do
	echo ""
	echo "Tile size 1000 x 1500"
	rm /tmp/S1A_IW_SLC__1SDV_20190715T160437_20190715T160504_028130_032D5B_58D6_Orb_Stack_coh_deb_tc.tif 
	time ./alus --alg_name terrain-correction -i /root/alus_test_set/S1A_IW_SLC__1SDV_20190715T160437_20190715T160504_028130_032D5B_58D6_Orb_Stack_coh_deb.tif -o /tmp/S1A_IW_SLC__1SDV_20190715T160437_20190715T160504_028130_032D5B_58D6_Orb_Stack_coh_deb_tc.tif --aux /root/alus_test_set/S1A_IW_SLC__1SDV_20190715T160437_20190715T160504_028130_032D5B_58D6_Orb_Stack_coh_deb.data --dem /root/alus_test_set/dem/srtm_41_01.tif --dem /root/alus_test_set/dem/srtm_42_01.tif -x 1000 -y 1500

done

for i in 1 2 3 4 5
do
	echo ""
	echo "Tile size 5000 x 1500"
	rm /tmp/S1A_IW_SLC__1SDV_20190715T160437_20190715T160504_028130_032D5B_58D6_Orb_Stack_coh_deb_tc.tif 
	time ./alus --alg_name terrain-correction -i /root/alus_test_set/S1A_IW_SLC__1SDV_20190715T160437_20190715T160504_028130_032D5B_58D6_Orb_Stack_coh_deb.tif -o /tmp/S1A_IW_SLC__1SDV_20190715T160437_20190715T160504_028130_032D5B_58D6_Orb_Stack_coh_deb_tc.tif --aux /root/alus_test_set/S1A_IW_SLC__1SDV_20190715T160437_20190715T160504_028130_032D5B_58D6_Orb_Stack_coh_deb.data --dem /root/alus_test_set/dem/srtm_41_01.tif --dem /root/alus_test_set/dem/srtm_42_01.tif -x 5000 -y 1500

done

for i in 1 2 3 4 5
do
	echo ""
	echo "Tile size 10000 x 1500"
	rm /tmp/S1A_IW_SLC__1SDV_20190715T160437_20190715T160504_028130_032D5B_58D6_Orb_Stack_coh_deb_tc.tif 
	time ./alus --alg_name terrain-correction -i /root/alus_test_set/S1A_IW_SLC__1SDV_20190715T160437_20190715T160504_028130_032D5B_58D6_Orb_Stack_coh_deb.tif -o /tmp/S1A_IW_SLC__1SDV_20190715T160437_20190715T160504_028130_032D5B_58D6_Orb_Stack_coh_deb_tc.tif --aux /root/alus_test_set/S1A_IW_SLC__1SDV_20190715T160437_20190715T160504_028130_032D5B_58D6_Orb_Stack_coh_deb.data --dem /root/alus_test_set/dem/srtm_41_01.tif --dem /root/alus_test_set/dem/srtm_42_01.tif -x 10000 -y 1500

done

