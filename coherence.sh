#!/bin/bash

# Optional - modify path to execute GPT
#PATH=$PATH:/path/to/gpt/bin/
# Optional - one can specify full path to GPT executable if it is not in $PATH
#GPT_EXEC="~/snap/bin/gpt"
GPT_EXEC="/home/sven/snap7/bin/gpt"

function print_help {
    echo "Usage:"
    echo "./coherence.sh <input main> <input aftermath> <polarisation> <subswath main> <burst index main> <subswath aftermath> <burst index aftermath> <output_dir>"
}

if [[ "$#" != 8 ]]; then
    echo "Wrong count of input arguments"
    print_help
    exit 1
fi

set -e

input1=$1
input2=$2
polarisation=${3^^}
subswath_main=${4^^}
burst_nr_main=$5
subswath_aftermath=${6^^}
burst_nr_aftermath=$7
output_dir=$(realpath $8)
main_product_folder=$(basename $(dirname $input1))
main_product=${main_product_folder/\.SAFE/}

if ! echo "$burst_nr_main" | grep -qE '^[0-9]+$'; then
    echo "Burst index argument not a valid number."
    print_help
    exit 2
fi

if (( burst_nr_main < 1 || burst_nr_main > 9)); then
    echo "Burst index out of range [1, 9]."
    print_help
    exit 3
fi

if ! echo "$burst_nr_aftermath" | grep -qE '^[0-9]+$'; then
    echo "Burst index argument not a valid number."
    print_help
    exit 2
fi

if (( burst_nr_aftermath < 1 || burst_nr_aftermath > 9)); then
    echo "Burst index out of range [1, 9]."
    print_help
    exit 3
fi

graph_output="${output_dir}/${main_product}_${subswath_main}_${burst_nr_main}_${polarisation}_orb_stack_cor_deb.dim"
tif_output=${graph_output/\.dim/\.tif}
$GPT_EXEC orb_split_backgeo_deburst_graph.xml -Pinput1=$input1 -Pinput2=$input2 -Ppolarisation=$polarisation -Pburst_index_main=$burst_nr_main -Psubswath_main=$subswath_main -Pburst_index_aftermath=$burst_nr_aftermath -Psubswath_aftermath=$subswath_aftermath -Poutput=$graph_output -Ptif_output=$tif_output

cd alus

coh_output="${tif_output/\.tif/}_coh.tif"
aux_dir=${graph_output/\.dim/\.data}
./alus --alg_name coherence -i $tif_output -o $coh_output -x 1000 -y 1000 --aux $aux_dir
tc_output="${coh_output/\.tif/}_tc.tif"
./alus --alg_name terrain-correction -i $coh_output -o $tc_output -x 20 -y 20 --aux $aux_dir
